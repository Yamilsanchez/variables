#include<stdio.h>

 int main (int arg, char *argv[]){

     printf("El tamaño en bytes de la variable int es: %lu\n", sizeof (int));
     printf("El tamaño en bytes de la variable float es: %lu\n", sizeof (float));
     printf("El tamaño en bytes de la variable char es: %lu\n", sizeof (char));
     printf("El tamaño en bytes de la variable long in es: %lu\n", sizeof (long int));

     return 0;
}
